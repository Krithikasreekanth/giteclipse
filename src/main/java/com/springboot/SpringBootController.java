package com.springboot;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SpringBootController {
@RequestMapping("/")
public String home(Map<String,Object> model)
{
	model.put("message","Hi");
	return "Index";
}
@RequestMapping("next")
public String next(Map<String,Object> model)
{
	model.put("message","Hello,how are u");
	return "Next";
}
@Value("${welcome.message}")
private String messages = "";

                @RequestMapping("includecss")
                public String welcome(Map<String, Object> model) {
                                model.put("message",messages);
                                return "welcome";
                }

}
